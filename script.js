"use strict";

// 1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, якщо рядок є паліндромом
// (читається однаково зліва направо і справа наліво), або false в іншому випадку.


function isPalindrome(str) {
    
    str = str.toLowerCase().replace(/[^a-zа-я]/g, '');
    
    let palindrome = str === str.split('').reverse().join('');
    
    if (palindrome) {
        console.log("Є паліндромом.");
    } else {
        console.log("Рядок не є паліндромом.");
    }
    
    return palindrome;
}

let row = "Нави стали победителями мажора";
console.log(isPalindrome(row));

// 2. Створіть функцію, яка перевіряє довжину рядка. Вона приймає рядок, який потрібно перевірити, максимальну довжину і повертає true, якщо рядок менше або дорівнює вказаній довжині, і false, якщо рядок довший. Ця функція стане в нагоді для валідації форми. Приклади використання функції:
// Рядок коротше 20 символів
// funcName('checked string', 20); // true
// // Довжина рядка дорівнює 18 символів
// funcName('checked string', 10); // false


function checkStringLength(str, maxLength) {
    return str.length <= maxLength;
}

console.log(checkStringLength('checked string', 20)); 
console.log(checkStringLength('checked string', 10)); 

// 3. Створіть функцію, яка визначає скільки повних років користувачу. Отримайте дату народження користувача через prompt. Функція повина повертати значення повних років на дату виклику функцію.


function calculateAge() {
    let birthDateInput = prompt("Введіть дату вашого народження у форматі 'рік-місяць-день', наприклад '2000-01-01':");
    let birthDate = new Date(birthDateInput); 

    let currentDate = new Date();
    
    let ageInMilliseconds = currentDate - birthDate;

    let ageInYears = Math.floor(ageInMilliseconds / (1000 * 60 * 60 * 24 * 365)); 

    return ageInYears;
}

let age = calculateAge();
console.log("Ваш вік: " + age + " років.");